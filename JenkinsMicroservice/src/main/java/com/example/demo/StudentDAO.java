package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StudentDAO {
	
	List<StudentBean> list;
	
	public List<StudentBean> getStudents() {
		list = new ArrayList<>();
		list.add(new StudentBean("Sudarshan","22","69"));
		list.add(new StudentBean("Bucha","21","69696"));
		return list;
	}

}
