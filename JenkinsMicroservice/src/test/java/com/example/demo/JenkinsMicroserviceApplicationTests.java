package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JenkinsMicroserviceApplicationTests {

	@Autowired
	StudentDAO sdao;

	List<StudentBean> list;

	@Test
	public void check() {
		list = new ArrayList<>();
		list.add(new StudentBean("Sudarshan", "22", "69"));
		list.add(new StudentBean("Bucha", "21", "69696"));
		Assert.assertEquals(list.get(0) + " " + list.get(1), sdao.getStudents().get(0) + " " + sdao.getStudents().get(1));
	}

}
